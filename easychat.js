//---- Constants / Variables

const SERVER_PORT = 30000;

const MSG_USER = 'user';
const MSG_STATUS = 'status';
const MSG_CHAT = 'chat';

let clients = new Map();
let messages = [];


//---- Init


const syncReader = require("readline-sync");

//---- Parse arguments

var myArgs = process.argv.slice(2);

console.log("EasyChat. Quit with Ctrl+c.");

if (myArgs.length == 0) {
  console.log("Chat server mode, listening on port " + SERVER_PORT);
  startServerWith(SERVER_PORT);
}
else {
  var serverUrl = tidyUrlArgument(myArgs[0]);
  console.log("Chat client mode, connecting to " + serverUrl + ":" + SERVER_PORT);
  startClientWith(serverUrl, SERVER_PORT);
}


//---- Functions

function startServerWith(port) {
  const app = require('express')();
  const http = require('http').Server(app);
  const io = require('socket.io')(http);

  io.on('connect', (socket) => {

    socket.on(MSG_USER, (username) => {
      // Once a client announces its identity, add it to the clients list and broadcast the information to all clients.
      clients.set(socket.id, username);
      let statusMessage = `--- User '${username}' joined. Currently online: ` + Array.from(clients.values()).join(', ');
      console.log(statusMessage);
      socket.broadcast.emit(MSG_STATUS, statusMessage);
      socket.emit(MSG_CHAT, messages.join('\n'));
      socket.emit(MSG_STATUS, statusMessage);
    });

    socket.on(MSG_CHAT, (message) => {
      let username = clients.get(socket.id);
      let chatMessage = `[${username}] ${message}`
      console.log(chatMessage);
      messages.push(chatMessage);
      socket.broadcast.emit(MSG_CHAT, chatMessage);
      socket.emit(MSG_CHAT, chatMessage);
    });

    socket.on('disconnect', (reason) => {
      let username = clients.get(socket.id);
      clients.delete(socket.id);
      let statusMessage = `--- User '${username}' left. Currently online: ` + Array.from(clients.values()).join(', ');
      console.log(statusMessage);
      socket.broadcast.emit(MSG_STATUS, statusMessage);
    });

  });

  http.listen(SERVER_PORT, () => {
    console.log('Now accepting users.');
  });
}

function startClientWith(serverUrl, port) {

  // Enter name:
  var name = askForName();

  const io = require("socket.io-client");
  const socket = io(serverUrl + ":" + port);

  socket.on('connect', () => {
    console.log("Connected with " + socket.io.uri);
    socket.emit(MSG_USER, name);
  });

  socket.on('disconnect', () => {
    console.log("Server terminated. Quitting.");
    process.exit();
  });

  socket.on(MSG_STATUS, (data) => {
    console.log(data);
  });

  socket.on(MSG_CHAT, (data) => {
    console.log(data);
  });

  // Start listening to chat messages.
  const readline = require('readline');
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });
  rl.setPrompt('');
  rl.prompt();

  rl.on('line', function(line) {
    socket.emit(MSG_CHAT, line);
    rl.prompt();
  }).on('close', function() {
    process.exit(0);
  });
}

function askForName() {
  return syncReader.question("Name: ");
}

function tidyUrlArgument(urlArgument) {
  if (!urlArgument.startsWith("http://") || !urlArgument.startsWith("https://")) {
    urlArgument = "http://" + urlArgument;
  }
  return urlArgument;
}
