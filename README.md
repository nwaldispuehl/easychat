# EasyChat

Dockerized CLI chat application based on [node.js](https://nodejs.org/) / [socket.io](https://socket.io/) for demonstration purposes.

It communicates on HTTP port 30000 -- unencrypted -- and believes everthing you send it.
Depending on the arguments, it starts in server or client mode.

## Build Docker image

    $ docker build -t easychat .

## Usage

### Start client

To start the container as client, the URL of the server (e.g. `SERVER_URL`) has to be provided as argument:

    $ docker run -ti --rm easychat SERVER_URL

The terminal/TTY (`-t`) and interactive (`-i`) flags are needed so it remains attached and accepts user input.
We add the remove (`--rm`) directive so the container is removed again after termination.

This is how it looks after start. It first prompts for your name and then you just can write messages:

    EasyChat. Quit with Ctrl+c.
    Chat client mode, connecting to http://SERVER_URL:30000
    Name: Peter Parker
    Connected with http://SERVER_URL:30000

    --- User 'Peter Parker' joined. Currently online: Peter Parker
    Hi, is there something interesting going on?
    [Peter Parker] Hi, is there something interesting going on?

### Start server

To start the container as server, it can be started without arguments.

    $ docker run -d -p 30000:30000 --name easychat easychat

We start it detached (`-d`) and with the port 30000 published to the host.

This is how the server looks after start. All user changes and messages are logged:

    EasyChat. Quit with Ctrl+c.
    Chat server mode, listening on port 30000
    Now accepting users.
    --- User 'Peter Parker' joined. Currently online: Peter Parker
    [Peter Parker] Hi, is there something interesting going on?
