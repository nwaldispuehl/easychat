FROM node:12-alpine

ADD package.json /srv
ADD package-lock.json /srv
ADD easychat.js /srv
ADD start_easychat.sh /srv

WORKDIR /srv

RUN npm install --silent

EXPOSE 30000

ENTRYPOINT ["./start_easychat.sh"]
